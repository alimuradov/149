// frontend/src/store/index.js
import Vue from 'vue'
import Vuex from 'vuex'
import { Page } from '../api/pages'
import { SET_PAGES } from './mutation-types.js'

Vue.use(Vuex)

// Состояние
const state = {
  pages: [] // список страниц
}

// Геттеры
const getters = {
  pages: state => state.pages  // получаем список заметок из состояния
}

// Мутации
const mutations = {
  // Задаем список заметок
  [SET_PAGES] (state, { pages }) {
    state.notes = pages
  }
}

// Действия
const actions = {
  getPages ({ commit }) {
    Page.list().then(pages => {
      commit(SET_PAGES, { pages })
    })
  }
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})
