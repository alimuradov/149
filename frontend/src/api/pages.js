import { HTTP } from './common'

export const Page = {
  detail (slug) {
    return HTTP.post('/pages/:slug').then(response => {
      return response.data
    })
  },
  list () {
    return HTTP.get('/pages/').then(response => {
      return response.data
    })
  }
}
