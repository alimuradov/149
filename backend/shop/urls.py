from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static

from catalog.views import PillList, data_fill, search


urlpatterns = [
    url(r'^api/v1/', include('catalog.urls')),
    path('admin/', admin.site.urls),
    path('summernote/', include('django_summernote.urls')),
    url(r'^pills/$', PillList.as_view()),
    url(r'^search/$', search),
    url(r'^api/v1/datafill/$', data_fill),
]


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
 