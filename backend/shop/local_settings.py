import os

from django.conf import settings
from django.conf.urls.static import static

from urls import urlpatterns

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

from .settings import INSTALLED_APPS, MIDDLEWARE

DEBUG=True

ALLOWED_HOSTS = ['127.0.0.1', 'localhost', '192.168.88.148', '192.168.88.208', '*', '0.0.0.0'] 

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': '149',
        'USER': '149',
        'PASSWORD': '149',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

if DEBUG:   
    INSTALLED_APPS += (
        'debug_toolbar',
    )

    MIDDLEWARE += (
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    )
