from django.db import models


class Pharmacy(models.Model):
    title = models.CharField('Название', max_length=128)
    address = models.CharField('Адрес', max_length=128)
    phone_number = models.CharField('Номер телефона', max_length=32)
    pharm_code = models.CharField('Код Аптеки', max_length=15, unique=True, blank=False, null=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Аптека'
        verbose_name_plural = 'Аптеки'


class Pill(models.Model):
    pharmacy = models.ForeignKey(Pharmacy, verbose_name='Аптека', on_delete=models.CASCADE)
    item_code = models.CharField('Код номенклатуры', max_length=50)
    title = models.CharField('Название', max_length=256)
    batch_number = models.CharField('Партия', max_length=256)
    price = models.FloatField('Цена')
    balance = models.FloatField('Остаток')
    last_update = models.DateTimeField('Обновлено', auto_now=True)
    is_active = models.BooleanField('Активный', default=True)


    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Препарат'
        verbose_name_plural = 'Препараты'